"""
albs_oval_errata_diff.py is a service startup script
"""
import sys
from albs_oval_errata_diff.start import start

try:
    YAML_PATH = sys.argv[1]
except IndexError:
    print(f"Usage: {sys.argv[0]} config.yml")
    sys.exit(1)
start(YAML_PATH)
