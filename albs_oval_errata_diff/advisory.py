"""
advisory contains Advisory dataclass definition
"""
from dataclasses import dataclass
from typing import List

from .package import Package


@dataclass
class Advisory:
    """
    Represents Secutity/Bug/Enhancment advisory deffition extracted
    from oval or errata

    Params
    ------
    title: RHBA-2022:5749: .NET 6.0 bugfix update (Moderate)
    advisory_type: RHBA
    id: 2022-5749
    """
    title: str
    advisory_type: str
    id: str  # pylint: disable=invalid-name
    packages: List[Package]
