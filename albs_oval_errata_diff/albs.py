"""
albs.py contains ALBS class
"""
from urllib.parse import urljoin
from typing import Union, Dict

import requests


class ALBS:
    """
    ALBS class implemets buildsys.almalinux.org API interaction logic
    """

    def __init__(self, url: str, token: str, timeout: int):
        self.url = url
        self.token = token
        self.timeout = timeout
        self._platforms = self._get_platforms()

    def _get_platforms(self) -> Dict[str, int]:
        '''
        Getting list of all platforms and
        return Dict: platform_name -> platform_id
        '''
        endpoint = '/api/v1/platforms/'
        headers = {'accept': 'application/json',
                   'Authorization': f'Bearer {self.token}'}
        response = requests.get(url=urljoin(self.url, endpoint),
                                headers=headers,
                                timeout=self.timeout)
        response.raise_for_status()
        res = {platform['name']: platform['id']
               for platform in response.json()}
        return res

    def get_errata_status(self, errata_id: str, platform_name: str) -> Union[str, None]:
        """
        Get release status for particular errata_id
        Params
        ------
            errata_id: str: errata id to get (ALSA-2023:0095)
        Returns
        -------
            str: release status
            If errata_id was not found Returns None
        Raises
        ------
            Any errors raised by requests libary
            ValueError if platform_name not found in buildsys
        """
        endpoint = '/api/v1/errata/query/'
        # platformId
        try:
            platform_id = self._platforms[platform_name]
        except KeyError as error:
            raise ValueError(f'{platform_name} was not found') from error
        params = {'id': errata_id, 'platformId': platform_id}
        headers = {'accept': 'application/json',
                   'Authorization': f'Bearer {self.token}'}
        response = requests.get(url=urljoin(self.url, endpoint),
                                params=params, headers=headers,
                                timeout=self.timeout)
        response.raise_for_status()
        response_json = response.json()

        # errata_id was not found
        if response_json['total_records'] == 0:
            return
        return response_json['records'][0]['release_status']
