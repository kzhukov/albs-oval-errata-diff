"""
package.py contains Package dataclass definition
"""
from dataclasses import dataclass


@dataclass
class Package:
    """
    Package represents RPM package extracted from RHEL/ALMA OVAL/Errata files
    """
    name: str
    version: str

    def __str__(self):
        return f"{self.name}-{self.version}"
