# albs-oval-errata-diff

Service compares RHEL Oval with AlmaLinux Oval/Errata and stores differences. Differences are available via HTTP GET request in form of JSON

## Components
### comparer
Downloads Oval/Errata files and generates differences report

### webserver
Publishes JSON report via aiohttp webserver

## Configuration
check [config.default.yml](config.default.yml) for references

## Requirements
- Python 3.9
- pip
- virtualenv

## Installation
1. Checkout code
    ```bash
    $ git clone git@git.almalinux.org:kzhukov/albs-oval-errata-diff.git
    ```
2. Create and initialize virtual enviroment
    ```bash
    $ virtualenv -p python3.9 venv && source venv/bin/activate
    ```
3. Install requirements
    ```bash
    $ pip install -r requirements.txt
    ```
4. Create config file using [config.default.yml](config.default.yml) and start service with _albs_oval_errata_diff.py_ script
    ```bash
    $ python albs_oval_errata_diff.py config.yml
    2022-12-29 16:20:11,139 INFO start Trying to load diff file from disk
    2022-12-29 16:20:11,142 INFO start Diff file was loaded
    2022-12-29 16:20:11,142 INFO start Starting diff_checker in background
    2022-12-29 16:20:11,143 INFO diff_checker Start comparing
    2022-12-29 16:20:11,143 INFO start Starting webserver
    2022-12-29 16:20:11,144 INFO comparer_run Processing release 8
    2022-12-29 16:20:11,148 INFO comparer_run downloading rhel oval
    ======== Running on http://127.0.0.1:3001 ========
    (Press CTRL+C to quit)
    2022-12-29 16:20:12,142 INFO comparer_run parsing rhel oval
    2022-12-29 16:20:13,154 INFO comparer_run downloading alma oval
    2022-12-29 16:20:16,516 INFO comparer_run parsing alma oval
    2022-12-29 16:20:17,695 INFO comparer_run downloading alma errata
    2022-12-29 16:20:28,894 INFO comparer_run parsing alma errata
    2022-12-29 16:20:29,143 INFO comparer_run comparing rhel and alma
    2022-12-29 16:20:29,233 INFO comparer_run Processing release 9
    2022-12-29 16:20:29,234 INFO comparer_run downloading rhel oval
    2022-12-29 16:20:29,599 INFO comparer_run parsing rhel oval
    2022-12-29 16:20:29,716 INFO comparer_run downloading alma oval
    2022-12-29 16:20:31,033 INFO comparer_run parsing alma oval
    2022-12-29 16:20:31,165 INFO comparer_run downloading alma errata
    2022-12-29 16:20:33,542 INFO comparer_run parsing alma errata
    2022-12-29 16:20:33,601 INFO comparer_run comparing rhel and alma
    2022-12-29 16:20:33,621 INFO diff_checker Finished comparing, updating diff dict
    2022-12-29 16:20:33,622 INFO diff_checker Saving results to disk
    2022-12-29 16:20:33,630 INFO diff_checker Done
    2022-12-29 16:20:33,630 INFO diff_checker Finished comparing, go to sleep for 30 minutes
    ```